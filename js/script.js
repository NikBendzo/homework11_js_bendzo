"use strict";

document.addEventListener("DOMContentLoaded", function () {
  const form = document.querySelector(".password-form");

  form.addEventListener("click", function (event) {
    let target = event.target;

    if (target.classList.contains("icon-password")) {
      let input = target.previousElementSibling;
      togglePasswordVisibility(input, target);
    }

    if (target.classList.contains("btn")) {
      event.preventDefault();
      validatePasswords();
    }
  });

  function togglePasswordVisibility(input, icon) {
    if (input.type === "password") {
      input.type = "text";
      icon.classList.remove("fa-eye");
      icon.classList.add("fa-eye-slash");
    } else {
      input.type = "password";
      icon.classList.remove("fa-eye-slash");
      icon.classList.add("fa-eye");
    }
  }

  function validatePasswords() {
    const passwordInputs = document.querySelectorAll(".input-wrapper input");
    const firstPassword = passwordInputs[0].value;
    const secondPassword = passwordInputs[1].value;

    const errorMessage = document.querySelector(".error-message");
    if (errorMessage) {
      errorMessage.remove();
    }

    if (firstPassword === secondPassword) {
      alert("You are welcome!");
    } else {
      displayErrorMessage("Потрібно ввести однакові значення");
    }
  }

  function displayErrorMessage(message) {
    const errorMessage = document.createElement("p");
    errorMessage.textContent = message;
    errorMessage.style.color = "red";
    errorMessage.classList.add("error-message");
    form.appendChild(errorMessage);
  }
});
